
//var star = document.getElementsByClassName("btn-star");
const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;



var currentTab = 0; // Current tab is set to be the first tab (0)

// Function for toggling content
function toggleContent() {
  var shortTxt = $(this).parent().find(".short-text"); // Find short text
  var longText = $(this).parent().find(".long-text"); // Find long text

  var verMas = "Ver más";
  var verMenos = "Ver menos";
  if ($(this).text() == verMas){
    $(this).text(verMenos);
  }else{
    $(this).text(verMas)
  }


  // Toggle visibility
  shortTxt.toggleClass("hidden");
  longText.toggleClass("hidden");
}


$(document).ready(function() {

  var verMasBtn = $('.ver-mas'); // Collect all buttons

  $('.ver-mas').each(function (index, value){ 
    $(value).on("click", toggleContent); // Bind toggle function to each button click event
  })
  

  /* Estrellas */ 
  var estrellas = $('.star-ratings-top');
    if (estrellas.length > 1){
      estrellas.each(function(index, value) {
        
        var dataAtrribute = value.dataset["attribute"] * 20;
        $(value).width(dataAtrribute + '%');
      });
    }
  /*Esconder topbar on scroll*/
  window.onscroll = function() {navbarScroll()};

  function navbarScroll() {
    if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 30) {
      $(".navbar").addClass("scrollDown");
      $(".navbar").removeClass("scrollUp");
    }else{
      $(".navbar").removeClass("scrollDown");
      $(".navbar").addClass("scrollUp");
    }
  }



  showTab(currentTab); // Display the current tab
  var activeTab = $(".tab:visible");
  var formInputs = activeTab.find("input");
  var textarea = activeTab.find("textarea");
  //var formDropdown = activeTab.find('.inputState');    
  formInputs.each(function(index,value){
    if ($("#"+value.id).attr("type") == "radio"){
        $("#"+value.id).on('click', function(){validarCheckbox(value.name)});
    }else{
      $("#"+value.id).on('keyup', function(){validateInput(value.id)});
    }
  });

  textarea.each(function(index,value){
    if ($("#"+value.id).length){
      $("#"+value.id).on('keyup', function(){validarTextarea(value.id)}); 
    }
  });
  


/*
  formDropdown.each(function(index,value){
   $("#"+value.id).on('change', function(){ 
     validarDropdown(value.id);
      claseInvalid();
    });
  });

 var isChecked = function() {
    for (var i = 0; i < checkbox.length; i++) {
      checkbox[i].addEventListener('click', isChecked);
    }
  }
*/
  // contador
  $('.counter').each(function () {
    $(this).prop('Counter',0).animate({
      Counter: $(this).text()
    }, 
    {
      duration: 3000,
      easing: 'swing',
      step: function (now) {
        $(this).text(Math.ceil(now));
      }
    });
  });


});
function validateInput(id) {
  var inputElement = $("#"+id);
  //var textarea = ('textarea#resena');
  let mensajeError = "";
  switch (id){
    case "email":
      if (!inputElement.val()) {
        // and set the current valid status to false:
        mensajeError = "El campo "+id+" esta vacio";
        break;
      }
      if (!emailRegex.test(String(inputElement.val().toLowerCase()))){
        mensajeError = "Ingrese un "+id+" válido";
      }      
    break;
    default:
      if (!inputElement.val()) {
        // and set the current valid status to false:
        mensajeError = "El campo "+id+" esta vacio";
      }
    break;  
  }

  //seteo el mensaje de error
  if (mensajeError) {
    inputElement.parent().addClass("invalid");
    setErrorSpan(id, mensajeError);
  }else {
    inputElement.parent().removeClass("invalid");
    setErrorSpan(id, "");
  }
}

/*
var isClicked = function() {
  var value = this.getAttribute("data-value");
  
  for (var i = 0; i < star.length; i++) {
    if (star[i].getAttribute("data-value") <= value) {
      star[i].classList.add("btn-filled");
    } else {
      star[i].classList.remove("btn-filled");
    }
  }
};

for (var i = 0; i < star.length; i++) {
  star[i].addEventListener('click', isClicked);
}
*/



var suggestions = [
    { value: 'Administración', data: 'AD' },
    // ...
    { value: 'Psicología', data: 'ZZ' }
    ];

    $('.search-autocomplete').autocomplete({
      lookup: suggestions,
      onSelect: function (suggestion) {
        console.log('You selected: ' + suggestion.value + ', ' + suggestion.data);
      }
    });
    $('#jumbo-search-autocomplete').autocomplete({
      lookup: suggestions,
      onSelect: function (suggestion) {
        console.log('You selected: ' + suggestion.value + ', ' + suggestion.data);
      }
    });




$(function() {      
    let isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;

    if (isMobile) {
      $('.news-carousel').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      });
    } else {
      $('.news-carousel').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      dots: true,
      });
    }
 });

 $(function() {      
    let isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;

    if (isMobile) {
      $('.xp-carousel').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2500,
      });
    } else {
      $('.xp-carousel').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2500,
      });
    }
 });

    $(document).on("scroll", function(){

    if ($(document).scrollTop() > 300){
      $("nav").removeClass("py-3");
      $("#nav-searchbar").removeClass("nav-searchbar-hidden");
    } else {
      $("nav").addClass("py-3");
      $("#nav-searchbar").addClass("nav-searchbar-hidden");
    }

  });

  if ('WebSocket' in window) {
		(function () {
			function refreshCSS() {
				var sheets = [].slice.call(document.getElementsByTagName("link"));
				var head = document.getElementsByTagName("head")[0];
				for (var i = 0; i < sheets.length; ++i) {
					var elem = sheets[i];
					var parent = elem.parentElement || head;
					parent.removeChild(elem);
					var rel = elem.rel;
					if (elem.href && typeof rel != "string" || rel.length == 0 || rel.toLowerCase() == "stylesheet") {
						var url = elem.href.replace(/(&|\?)_cacheOverride=\d+/, '');
						elem.href = url + (url.indexOf('?') >= 0 ? '&' : '?') + '_cacheOverride=' + (new Date().valueOf());
					}
					parent.appendChild(elem);
				}
			}
			var protocol = window.location.protocol === 'http:' ? 'ws://' : 'wss://';
			var address = protocol + window.location.host + window.location.pathname + '/ws';
			var socket = new WebSocket(address);
			socket.onmessage = function (msg) {
				if (msg.data == 'reload') window.location.reload();
				else if (msg.data == 'refreshcss') refreshCSS();
			};
			if (sessionStorage && !sessionStorage.getItem('IsThisFirstTime_Log_From_LiveServer')) {
				console.log('Live reload enabled.');
				sessionStorage.setItem('IsThisFirstTime_Log_From_LiveServer', true);
			}
		})();
	}
	else {
		console.error('Upgrade your browser. This Browser is NOT supported WebSocket for Live-Reloading.');
  }
  

  
  function showTab(n) {
    // This function will display the specified tab of the form ...
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    // ... and fix the Previous/Next buttons:
    if (n == 0) {
      document.getElementById("prevBtn").style.display = "none";
    } else {
      document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == (x.length - 1)) {
      document.getElementById("nextBtn").innerHTML = "Enviar";
    } else {
      document.getElementById("nextBtn").innerHTML = "Siguiente";
    }
    // ... and run a function that displays the correct step indicator:
    fixStepIndicator(n)
  }
  
  function nextPrev(n) {
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("tab");
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm()) return false;
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form... :
    if (currentTab >= x.length) {
      //...the form gets submitted:
      document.getElementById("regForm").submit();
      return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);

    //recargar validacion
    var activeTab = $(".tab:visible");
    var formInputs = activeTab.find("input");
    var textarea = activeTab.find("textarea");
    //var formDropdown = activeTab.find('.inputState');


    formInputs.each(function(index,value){
      if ($("#"+value.id).attr("type") == "radio"){
        $("#"+value.id).on('click', function(){validarCheckbox(value.name)});
        
      }else{
        $("#"+value.id).on('keyup', function(){validateInput(value.id)});
      }
    });
    
    
    textarea.each(function(index,value){
      if ($("#"+value.id).length){
        $("#"+value.id).on('keyup', function(){validarTextarea(value.id)}); 
      }
    });
    

  }
  
  function validarCheckbox(name) {
    if ($(".form-check-inline input[name='"+name+"']").is(':checked')) {
      $(".form-check-inline input[name='"+name+"']").closest('.group-radios').removeClass("invalid");
      setErrorSpan(name, "");
      return true; 
    }else {
      $(".form-check-inline input[name='"+name+"']").closest('.group-radios').addClass("invalid");
      setErrorSpan(name, "Campo obligatorio");
      return false;
    }
  };
  

  function validarTextarea(id){
    var textarea = $("#"+id);
    let mensajeError = "";
    if (!textarea.val()){
      mensajeError = "El campo "+id+" esta vacio";
    }
    if (textarea.val().length < 160){
      console.log(textarea.val().length < 160);
      mensajeError = "Debes ingresar más de 160 caracteres";
    }

    //seteo el mensaje de error
    if (mensajeError) {
      textarea.parent().addClass("invalid");
      setErrorSpan(id, mensajeError);
    }else {
      textarea.parent().removeClass("invalid");
      setErrorSpan(id, "");
    }
  }
/*
  function validarDropdown(id) {
    if ($('#'+id).val() === '') {
      setErrorSpan(id, "Campo obligatorio");
      return true;
    } else {
      setErrorSpan(id, "");
      return false;
    }
  }

  function claseInvalid() {
    var activeTab = $(".tab:visible");
    var formDropdown = activeTab.find('.inputState');    
    var dropdownVacio = false;
    
    formDropdown.each(function(index,value){
      if(validarDropdown(value.id)) {
        dropdownVacio = true;
      }
    });  
    if (dropdownVacio) {
      $('.group-inputs-dropdown').addClass("invalid");
    } else {
      $('.group-inputs-dropdown').removeClass("invalid");
    }
  }
*/
  function validateForm() {
  //  claseInvalid();
    var textarea = $('.tab:visible').find('textarea');
    if (textarea.length > 0){
      textarea.each(function(i,v){
        validarTextarea(v.id);
      })}

    let name = "";
    $('.tab:visible').find('input').each(function(i,v) {

      if ($("#"+v.id).attr("type") == "radio") {
        if (name != v.name) {
          name = v.name;
          validarCheckbox(v.name);
        }
      }else{
        validateInput(v.id);
      }
    })
    var hasErrors = $('.tab:visible').find('.invalid');

    var valid = hasErrors.length > 0 ? false : true;
    if (valid) {
        document.getElementsByClassName("step")[currentTab].className += " finish";
        // setear el tab
        
    }
    return valid; 
  } 

  function setErrorSpan(id, mensajeError){
    let errorSpan = $("#"+ id +"-error");

    errorSpan.text(mensajeError);
  }

  function fixStepIndicator(n) {
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
      x[i].className = x[i].className.replace(" active", "");
    }
    x[n].className += " active";
  }
